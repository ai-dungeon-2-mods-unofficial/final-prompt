from modloader import BaseMod
from story.story_manager import Story

class FinalPrompt(BaseMod):
    def initialization(self, generator, story_manager):
        def start_new_story(
            story_prompt, context="", game_state=None, upload_story=False
        ):
            story_manager.story = Story(
                context + story_prompt,
                context=context,
                game_state=game_state,
                upload_story=upload_story,
            )
            return str(story_manager.story)
        story_manager.start_new_story = start_new_story
